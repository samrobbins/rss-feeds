# RSS Feeds

A file for all the RSS feeds I subscribe to:

- [Vercel](https://vercel.com/atom)
- [Next.js](https://nextjs.org/feed.xml)
- [Paul Stamatiou](https://paulstamatiou.com/posts.xml)
- [Cassie Evans](https://www.cassie.codes/feed.xml)
- [viewBox newsletter](https://buttondown.email/viewBox/rss)
- [GatsbyJS](https://www.gatsbyjs.org/blog/rss.xml)
- [Tailwind CSS](https://blog.tailwindcss.com/feed.xml)
- [GitHub](https://github.blog/feed/)
- [Dan Abramov](https://overreacted.io/rss.xml)
- [Signal v. Noise](https://signalvnoise.com/posts.rss)

Podcasts:

- [Cortex](https://www.relay.fm/cortex/feed)
- [Full Stack Radio](https://feeds.simplecast.com/Gd37VcDw)
- [Rework](https://feeds.transistor.fm/rework)
- [Synthetic A Priori](https://feeds.transistor.fm/synthetic-a-priori)
- [Hello Internet](http://feeds.podtrac.com/rZy85ymZYZc$)
